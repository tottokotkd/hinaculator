extern crate rand;
extern crate hinaculator;

fn main() {
	use rand::{thread_rng, Rng};
	use hinaculator::skill::{Skill, ssr};
	use hinaculator::card::{SkillMock};
	use hinaculator::card::Unit;

	let performers: [_; 5] = [
		// score up
		SkillMock::new(Skill {effect: ssr::score_up(), threshold_rate: 40, interval: 4, length: 2, life_effect: 0}),

		// combo bonus
		SkillMock::new(Skill {effect: ssr::combo_bonus(), threshold_rate: 40, interval: 11, length: 5, life_effect: 0}),
		SkillMock::new(Skill {effect: ssr::combo_bonus(), threshold_rate: 40, interval: 7, length: 3, life_effect: 0}),

		// overload
		SkillMock::new(Skill {effect: ssr::overload(), threshold_rate: 35, interval: 7, length: 4, life_effect: -11}),

		// concentration
		SkillMock::new(Skill {effect: ssr::concentration(), threshold_rate: 40, interval: 11, length: 5, life_effect: 0}),

	];
	let mut unit = Unit::new(performers);
	unit.life(200);

	let music_length_secs = 120;
	let length_by_200ms = music_length_secs * 5;

	let mut rng = thread_rng();
	let notes: Vec<_> = (0..length_by_200ms).map(|_| rng.gen_range(0, 3)).collect();
	let rates: Vec<[i8; 5]> = (0..music_length_secs)
		.map(|_| [
			rng.gen_range(0, 101),
			rng.gen_range(0, 101),
			rng.gen_range(0, 101),
			rng.gen_range(0, 101),
			rng.gen_range(0, 101),
		]).collect();


	let mut i = 0;
	for sec in 0..music_length_secs {
		println!("sec. {}",sec);
		unit.activate(rates[sec]);

		let effect = unit.effect();
		println!("{}", effect);

		for _ in 0..5 {
			let note = notes[i];

			unit.add_time(200.0);
			i += 1;
		}
	}

}
