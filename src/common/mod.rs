use std::ops::BitAnd;
use std::cmp::{PartialOrd, Ordering};
use std::fmt::{Display, Formatter, Result};

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub enum Input {
	Perfect,
	Great,
	Good,
	Nice,
	Miss,
}

impl Default for Input {
	fn default() -> Self {
		Input::Great
	}
}

impl Display for Input {
	fn fmt(&self, f: &mut Formatter) -> Result {
		let name = match *self {
			Input::Perfect => "Perfect",
			Input::Great => "Great",
			Input::Good => "Good",
			Input::Nice => "Nice",
			Input::Miss => "Miss",
		};
		f.write_str(name)
	}
}


#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub enum SkillType {
	None,
	ScoreUp,
	ComboBonus,
	LifeRecovery,
	DamageGuard,
	ComboGuard,
	JudgementEnhancement,
	Overload,
	AllRound,
	Concentration,
	Merged,
}

impl Default for SkillType {
	fn default() -> Self {
		SkillType::None
	}
}

impl Display for SkillType {
	fn fmt(&self, f: &mut Formatter) -> Result {
		let name = match *self {
			SkillType::None => "none",
			SkillType::ScoreUp => "スコアアップ",
			SkillType::ComboBonus => "コンボボーナス",
			SkillType::LifeRecovery => "回復",
			SkillType::DamageGuard => "ダメージガード",
			SkillType::ComboGuard => "コンボ継続",
			SkillType::JudgementEnhancement => "判定強化",
			SkillType::Overload => "オーバーロード",
			SkillType::AllRound => "オールラウンド",
			SkillType::Concentration => "コンセントレーション",
			SkillType::Merged => "合算",
		};
		f.write_str(name)
	}
}

impl BitAnd for SkillType {
	type Output = Self;
	fn bitand(self, _: SkillType) -> Self {
		SkillType::Merged
	}
}

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub enum JudgementConversionMode {
	None,
	GreatToPerfect,
	NiceToPerfect,
	BadToPerfect,
}

impl Display for JudgementConversionMode {
	fn fmt(&self, f: &mut Formatter) -> Result {
		let name = match *self {
			JudgementConversionMode::None => "無",
			JudgementConversionMode::GreatToPerfect => "グレートをパーフェクトに",
			JudgementConversionMode::NiceToPerfect => "グレート・ナイスをパーフェクトに",
			JudgementConversionMode::BadToPerfect => "グレート・ナイス・バッドをパーフェクトに",
		};
		write!(f, "判定強化: {}", name)
	}
}

impl Default for JudgementConversionMode {
	fn default() -> Self {
		JudgementConversionMode::None
	}
}

impl PartialOrd for JudgementConversionMode {
	fn partial_cmp(&self, other: &JudgementConversionMode) -> Option<Ordering> {
		Some(self.cmp(other))
	}
}

impl Ord for JudgementConversionMode {
	fn cmp(&self, other: &Self) -> Ordering {
		let l: i32 = self.clone() as i32;
		let r: i32 = other.clone() as i32;
		l.cmp(&r)
	}
}

impl BitAnd for JudgementConversionMode {
	type Output = Self;
	fn bitand(self, rhs: JudgementConversionMode) -> Self {
		if self > rhs {
			self
		} else {
			rhs
		}
	}
}

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub enum ComboMode {
	SuccessWithPerfect,
	SuccessWithGreat,
	SuccessWithNice,
	SuccessWithBad,
}

impl Display for ComboMode {
	fn fmt(&self, f: &mut Formatter) -> Result {
		let name = match *self {
			ComboMode::SuccessWithPerfect => "パーフェクトでコンボ継続",
			ComboMode::SuccessWithGreat => "グレートでコンボ継続",
			ComboMode::SuccessWithNice => "ナイスでコンボ継続",
			ComboMode::SuccessWithBad => "バッドでコンボ継続",
		};
		f.write_str(name)
	}
}

impl Default for ComboMode {
	fn default() -> Self {
		ComboMode::SuccessWithGreat
	}
}

impl PartialOrd for ComboMode {
	fn partial_cmp(&self, other: &ComboMode) -> Option<Ordering> {
		Some(self.cmp(other))
	}
}

impl Ord for ComboMode {
	fn cmp(&self, other: &Self) -> Ordering {
		let l: i32 = self.clone() as i32;
		let r: i32 = other.clone() as i32;
		l.cmp(&r)
	}
}

impl BitAnd for ComboMode {
	type Output = Self;
	fn bitand(self, rhs: ComboMode) -> Self {
		if self > rhs {
			self
		} else {
			rhs
		}
	}
}
