use super::super::{JudgementConversionMode, ComboMode};
use super::{Skill, SkillEffect, util};

/// all skills (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::ssr::all;
/// let skills = all();
/// ```
pub fn all() -> [Skill; 25] {
	[
		// score up
		Skill {effect: score_up(), threshold_rate: 40, interval: 13, length: 6, life_effect: 0},
		Skill {effect: score_up(), threshold_rate: 40, interval: 11, length: 5, life_effect: 0},
		Skill {effect: score_up(), threshold_rate: 40, interval: 9, length: 4, life_effect: 0},
		Skill {effect: score_up(), threshold_rate: 40, interval: 7, length: 3, life_effect: 0},
		Skill {effect: score_up(), threshold_rate: 40, interval: 4, length: 2, life_effect: 0},
		Skill {effect: score_up(), threshold_rate: 35, interval: 11, length: 6, life_effect: 0},
		Skill {effect: score_up(), threshold_rate: 35, interval: 9, length: 5, life_effect: 0},
		Skill {effect: score_up(), threshold_rate: 35, interval: 6, length: 3, life_effect: 0},

		// combo bonus
		Skill {effect: combo_bonus(), threshold_rate: 40, interval: 13, length: 6, life_effect: 0},
		Skill {effect: combo_bonus(), threshold_rate: 40, interval: 11, length: 5, life_effect: 0},
		Skill {effect: combo_bonus(), threshold_rate: 40, interval: 9, length: 4, life_effect: 0},
		Skill {effect: combo_bonus(), threshold_rate: 40, interval: 7, length: 3, life_effect: 0},
		Skill {effect: combo_bonus(), threshold_rate: 35, interval: 9, length: 5, life_effect: 0},
		Skill {effect: combo_bonus(), threshold_rate: 35, interval: 7, length: 4, life_effect: 0},

		// life recovery
		Skill {effect: life_recovery(), threshold_rate: 40, interval: 13, length: 5, life_effect: 0},
		Skill {effect: life_recovery(), threshold_rate: 40, interval: 11, length: 4, life_effect: 0},
		Skill {effect: life_recovery(), threshold_rate: 40, interval: 10, length: 4, life_effect: 0},

		// judgement enhancement
		Skill {effect: judgement_enhancement(), threshold_rate: 40, interval: 15, length: 5, life_effect: 0},
		Skill {effect: judgement_enhancement(), threshold_rate: 40, interval: 12, length: 4, life_effect: 0},
		Skill {effect: judgement_enhancement(), threshold_rate: 40, interval: 9, length: 3, life_effect: 0},

		// overload
		Skill {effect: overload(), threshold_rate: 35, interval: 9, length: 5, life_effect: -15},
		Skill {effect: overload(), threshold_rate: 35, interval: 7, length: 4, life_effect: -11},
		Skill {effect: overload(), threshold_rate: 35, interval: 6, length: 3, life_effect: -9},

		// all round
		Skill {effect: all_round(), threshold_rate: 40, interval: 8, length: 3, life_effect: 0},

		// concentration
		Skill {effect: concentration(), threshold_rate: 40, interval: 11, length: 5, life_effect: 0},
	]
}

/// skill: score up (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::ssr::score_up;
/// let effect = score_up();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::ScoreUp,
/// #	p_score_up: 17,
///	#	g_score_up: 17,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn score_up() -> SkillEffect {
	util::score_up(17, 17)
}

/// skill: combo bonus (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::ssr::combo_bonus;
/// let effect = combo_bonus();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::ComboBonus,
/// #	p_score_up: 0,
///	#	g_score_up: 0,
/// #	combo_bonus: 15,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn combo_bonus() -> SkillEffect {
	util::combo_bonus(15)
}

/// skill: life recovery (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::ssr::life_recovery;
/// let effect = life_recovery();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::LifeRecovery,
/// #	p_score_up: 0,
///	# 	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 3,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn life_recovery() -> SkillEffect {
	util::life_recovery(3)
}

/// skill: damage guard (SSR)
///
/// # Example
///
/// ```should_panic
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::ssr::damage_guard;
/// let effect = damage_guard();
/// ```
pub fn damage_guard() -> SkillEffect {
	unreachable!()
}

/// skill: combo guard (SSR)
///
/// # Example
///
/// ```should_panic
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::ssr::combo_guard;
/// let effect = combo_guard();
/// ```
pub fn combo_guard() -> SkillEffect {
	unreachable!()
}

/// skill: judgement enhancement (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::ssr::judgement_enhancement;
/// let effect = judgement_enhancement();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::JudgementEnhancement,
/// #	p_score_up: 0,
///	# 	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::BadToPerfect,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn judgement_enhancement() -> SkillEffect {
	util::judgement_enhancement(JudgementConversionMode::BadToPerfect)
}

/// skill: overload (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::ssr::overload;
/// let effect = overload();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::Overload,
/// #	p_score_up: 18,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithBad,
/// # });
/// ```
pub fn overload() -> SkillEffect {
	util::overload(18, 0, ComboMode::SuccessWithBad)
}

/// skill: all round (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::ssr::all_round;
/// let effect = all_round();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::AllRound,
/// #	p_score_up: 0,
///	#	g_score_up: 0,
/// #	combo_bonus: 13,
/// #	recovery: 1,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn all_round() -> SkillEffect {
	util::all_round(13, 1)
}

/// skill: concentration (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::ssr::concentration;
/// let effect = concentration();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::Concentration,
/// #	p_score_up: 18,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithPerfect,
/// # });
/// ```
pub fn concentration() -> SkillEffect {
	util::concentration(18, 0, ComboMode::SuccessWithPerfect)
}
