use super::super::{JudgementConversionMode, ComboMode};
use super::{SkillEffect, util};

/// all skills (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::r::all;
/// let skills = all();
/// ```
pub fn all() -> [SkillEffect; 5] {
	[
		score_up(),
		combo_bonus(),
		life_recovery(),
		combo_guard(),
		judgement_enhancement(),
	]
}

/// skill: score up (R)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::r::score_up;
/// let effect = score_up();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::ScoreUp,
/// #	p_score_up: 10,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn score_up() -> SkillEffect {
	util::score_up(10, 0)
}

/// skill: combo bonus (R)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::r::combo_bonus;
/// let effect = combo_bonus();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::ComboBonus,
/// #	p_score_up: 0,
///	#	g_score_up: 0,
/// #	combo_bonus: 8,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn combo_bonus() -> SkillEffect {
	util::combo_bonus(8)
}

/// skill: life recovery (R)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::r::life_recovery;
/// let effect = life_recovery();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::LifeRecovery,
/// #	p_score_up: 0,
///	# 	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 2,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn life_recovery() -> SkillEffect {
	util::life_recovery(2)
}

/// skill: damage guard (R)
///
/// # Example
///
/// ```should_panic
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::r::damage_guard;
/// let effect = damage_guard();
/// ```
pub fn damage_guard() -> SkillEffect {
	unimplemented!()
}

/// skill: combo guard (R)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::r::combo_guard;
/// let effect = combo_guard();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::ComboGuard,
/// #	p_score_up: 0,
///	# 	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithNice,
/// # });
/// ```
pub fn combo_guard() -> SkillEffect {
	util::combo_guard(ComboMode::SuccessWithNice)
}

/// skill: judgement enhancement (R)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::r::judgement_enhancement;
/// let effect = judgement_enhancement();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::JudgementEnhancement,
/// #	p_score_up: 0,
///	# 	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::GreatToPerfect,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn judgement_enhancement() -> SkillEffect {
	util::judgement_enhancement(JudgementConversionMode::GreatToPerfect)
}

/// skill: overload (R)
///
/// # Example
///
/// ```should_panic
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::r::overload;
/// let effect = overload();
/// ```
pub fn overload() -> SkillEffect {
	unimplemented!()
}

/// skill: all round (R)
///
/// # Example
///
/// ```should_panic
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::r::all_round;
/// let effect = all_round();
/// ```
pub fn all_round() -> SkillEffect {
	unimplemented!()
}

/// skill: concentration (R)
///
/// # Example
///
/// ```should_panic
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::r::concentration;
/// let effect = concentration();
/// ```
pub fn concentration() -> SkillEffect {
	unimplemented!()
}
