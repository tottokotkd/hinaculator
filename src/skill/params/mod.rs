pub mod util;

use super::super::{Input, SkillType, ComboMode, JudgementConversionMode};

use std::ops::BitAnd;
use std::cmp::max;
use std::fmt::{Display, Formatter, Result};

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub struct Skill {

	// interval by seconds
	pub interval: i8,

	// threshold by rate
	pub threshold_rate: i8,

	/// length by seconds
	pub length: i8,

	/// skill effect
	pub effect: SkillEffect,

	/// skill effect on life (for overload)
	pub life_effect: i16,
}

impl Display for Skill {
	fn fmt(&self, f: &mut Formatter) -> Result {
		write!(f, "{} ({}秒毎{}%, {}秒)", self.effect.skill_type, self.interval, self.threshold_rate, self.length)
	}
}

#[derive(Eq, PartialEq, Debug, Clone, Hash, Default)]
pub struct SkillEffect {
	pub skill_type: SkillType,
	pub p_score_up: i8,
	pub g_score_up: i8,
	pub combo_bonus: i8,
	pub recovery: i8,
	pub guard: bool,
	pub conversion: JudgementConversionMode,
	pub combo: ComboMode,
}

impl SkillEffect {
	pub fn input(i: Input, life: i16, combo_count: u16) {

	}
}

impl BitAnd for SkillEffect {
	type Output = Self;
	fn bitand(self, rhs: SkillEffect) -> Self {
		SkillEffect {
			skill_type: self.skill_type & rhs.skill_type,
			p_score_up: max(self.p_score_up, rhs.p_score_up),
			g_score_up: max(self.g_score_up, rhs.g_score_up),
			combo_bonus: max(self.combo_bonus, rhs.combo_bonus),
			recovery: max(self.recovery, rhs.recovery),
			guard: self.guard & rhs.guard,
			conversion: self.conversion & rhs.conversion,
			combo: self.combo & rhs.combo,
		}
	}
}

impl Display for SkillEffect {
	fn fmt(&self, f: &mut Formatter) -> Result {
		match self.skill_type {
			SkillType::Merged => {
				write!(
					f,
					"Merged: スコア {}% | {}%, コンボ {}%, 回復 {}, ガード{}, {}, {}",
					self.p_score_up,
					self.g_score_up,
					self.combo_bonus,
					self.recovery,
					if self.guard {"有"} else {"無"},
					self.conversion,
					self.combo,
				)
			}

			_ => {
				self.skill_type.fmt(f)
			}
		}
	}
}
