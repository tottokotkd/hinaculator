use super::*;

pub fn score_up(perfect: i8, great: i8) -> SkillEffect {
	SkillEffect {
		skill_type: SkillType::ScoreUp,
		p_score_up: perfect,
		g_score_up: great,
		combo_bonus: 0,
		recovery: 0,
		guard: false,
		conversion: JudgementConversionMode::default(),
		combo: ComboMode::default(),
	}
}

pub fn combo_bonus(value: i8) -> SkillEffect {
	SkillEffect {
		skill_type: SkillType::ComboBonus,
		p_score_up: 0,
		g_score_up: 0,
		combo_bonus: value,
		recovery: 0,
		guard: false,
		conversion: JudgementConversionMode::default(),
		combo: ComboMode::default(),
	}
}

pub fn life_recovery(value: i8) -> SkillEffect {
	SkillEffect {
		skill_type: SkillType::LifeRecovery,
		p_score_up: 0,
		g_score_up: 0,
		combo_bonus: 0,
		recovery: value,
		guard: false,
		conversion: JudgementConversionMode::default(),
		combo: ComboMode::default(),
	}
}

pub fn damage_guard() -> SkillEffect {
	SkillEffect {
		skill_type: SkillType::DamageGuard,
		p_score_up: 0,
		g_score_up: 0,
		combo_bonus: 0,
		recovery: 0,
		guard: true,
		conversion: JudgementConversionMode::default(),
		combo: ComboMode::default(),
	}
}

pub fn combo_guard(combo: ComboMode) -> SkillEffect {
	SkillEffect {
		skill_type: SkillType::ComboGuard,
		p_score_up: 0,
		g_score_up: 0,
		combo_bonus: 0,
		recovery: 0,
		guard: false,
		conversion: JudgementConversionMode::default(),
		combo: combo,
	}
}

pub fn judgement_enhancement(conversion: JudgementConversionMode) -> SkillEffect {
	SkillEffect {
		skill_type: SkillType::JudgementEnhancement,
		p_score_up: 0,
		g_score_up: 0,
		combo_bonus: 0,
		recovery: 0,
		guard: false,
		conversion: conversion,
		combo: ComboMode::default(),
	}
}

pub fn overload(perfect: i8, great: i8, combo: ComboMode) -> SkillEffect {
	SkillEffect {
		skill_type: SkillType::Overload,
		p_score_up: perfect,
		g_score_up: great,
		combo_bonus: 0,
		recovery: 0,
		guard: false,
		conversion: JudgementConversionMode::default(),
		combo: combo,
	}
}

pub fn all_round(combo_bonus: i8, recovery: i8) -> SkillEffect {
	SkillEffect {
		skill_type: SkillType::AllRound,
		p_score_up: 0,
		g_score_up: 0,
		combo_bonus: combo_bonus,
		recovery: recovery,
		guard: false,
		conversion: JudgementConversionMode::default(),
		combo: ComboMode::default(),
	}
}

pub fn concentration(perfect: i8, great: i8, combo: ComboMode) -> SkillEffect {
	SkillEffect {
		skill_type: SkillType::Concentration,
		p_score_up: perfect,
		g_score_up: great,
		combo_bonus: 0,
		recovery: 0,
		guard: false,
		conversion: JudgementConversionMode::default(),
		combo: combo,
	}
}
