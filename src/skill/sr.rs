use super::super::{JudgementConversionMode, ComboMode};
use super::{SkillEffect, util};


/// all skills (SSR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::sr::all;
/// let skills = all();
/// ```
pub fn all() -> [SkillEffect; 8] {
	[
		score_up(),
		combo_bonus(),
		life_recovery(),
		damage_guard(),
		combo_guard(),
		judgement_enhancement(),
		overload(),
		concentration(),
	]
}

/// skill: score up (SR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::sr::score_up;
/// let effect = score_up();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::ScoreUp,
/// #	p_score_up: 15,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn score_up() -> SkillEffect {
	util::score_up(15, 0)
}

/// skill: combo bonus (SR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::sr::combo_bonus;
/// let effect = combo_bonus();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::ComboBonus,
/// #	p_score_up: 0,
///	#	g_score_up: 0,
/// #	combo_bonus: 12,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn combo_bonus() -> SkillEffect {
	util::combo_bonus(12)
}

/// skill: life recovery (SR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::sr::life_recovery;
/// let effect = life_recovery();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::LifeRecovery,
/// #	p_score_up: 0,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 3,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn life_recovery() -> SkillEffect {
	util::life_recovery(3)
}

/// skill: damage guard (SR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::sr::damage_guard;
/// let effect = damage_guard();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::DamageGuard,
/// #	p_score_up: 0,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: true,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn damage_guard() -> SkillEffect {
	util::damage_guard()
}

/// skill: combo guard (SR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::sr::combo_guard;
/// let effect = combo_guard();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::ComboGuard,
/// #	p_score_up: 0,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithNice,
/// # });
/// ```
pub fn combo_guard() -> SkillEffect {
	util::combo_guard(ComboMode::SuccessWithNice)
}

/// skill: judgement enhancement (SR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::sr::judgement_enhancement;
/// let effect = judgement_enhancement();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::JudgementEnhancement,
/// #	p_score_up: 0,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::NiceToPerfect,
/// #	combo: ComboMode::SuccessWithGreat,
/// # });
/// ```
pub fn judgement_enhancement() -> SkillEffect {
	util::judgement_enhancement(JudgementConversionMode::NiceToPerfect)
}

/// skill: overload (SR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::sr::overload;
/// let effect = overload();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::Overload,
/// #	p_score_up: 16,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithBad,
/// # });
/// ```
pub fn overload() -> SkillEffect {
	util::overload(16, 0, ComboMode::SuccessWithBad)
}

/// skill: all round (SR)
///
/// # Example
///
/// ```should_panic
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::sr::all_round;
/// let effect = all_round();
/// ```
pub fn all_round() -> SkillEffect {
	unreachable!()
}

/// skill: concentration (SR)
///
/// # Example
///
/// ```
/// # use hinaculator::skill::*;
/// # use hinaculator::skill::sr::concentration;
/// let effect = concentration();
/// # assert_eq!(effect, SkillEffect {
/// #	skill_type: SkillType::Concentration,
/// #	p_score_up: 16,
///	#	g_score_up: 0,
/// #	combo_bonus: 0,
/// #	recovery: 0,
/// #	guard: false,
/// #	conversion: JudgementConversionMode::None,
/// #	combo: ComboMode::SuccessWithPerfect,
/// # });
/// ```
pub fn concentration() -> SkillEffect {
	util::concentration(16, 0, ComboMode::SuccessWithPerfect)
}
