use std::fmt::{Display, Formatter, Result};
use super::skill::{Skill, SkillEffect};

pub struct Unit <P: Performer> {

	/// performers
	pub performers: [P; 5],

	/// current time (milli seconds)
	time: f32,

	/// effect limits (milli seconds)
	effect_limits: [f32; 5],

	/// life
	life: i16,

	/// next activation time
	next_activation_time: [f32; 5],
}

impl <P: Performer> Unit <P> {

	pub fn new(performers: [P; 5]) -> Self {
		let ns = [
			performers[0].interval(),
			performers[1].interval(),
			performers[2].interval(),
			performers[3].interval(),
			performers[4].interval(),
		];

		Unit {
			performers: performers,
			time: 0.0,
			effect_limits: [0.0; 5],
			life: 0,
			next_activation_time: ns,
		}
	}

	pub fn time(&mut self, t: f32) -> &mut Self {
		self.time = t;
		self
	}

	pub fn add_time(&mut self, t: f32) -> &mut Self {
		self.time += t;
		self
	}

	pub fn life(&mut self, l: i16) -> &mut Self {
		self.life += l;
		self
	}

	pub fn activate(&mut self, rates: [i8; 5]) -> &mut Self {
		println!("time: {}, life: {}", self.time, self.life);
		'lp: for n in 0..5 {

			let performer = &self.performers[n];

			{
				let mut next_time = &mut self.next_activation_time[n];

				if self.time < *next_time {
					println!("interval: until {}", *next_time);
					continue 'lp;
				}

				let interval = performer.interval();
				while *next_time < self.time {
					*next_time += interval;
				}
			}

			let rate = rates[n];
			match performer.activate(rate, self.life) {
				Some(Activity {length, life}) => {
					println!("{}: active: {} (rate = {}, length = {})", n + 1, life, rate, length);
					self.life = life;
					self.effect_limits[n] = self.time + length;
				},
				None => {
					println!("{}: no effect... (rate {})", n + 1, rate);
				},
			}
		}

		self
	}

	pub fn effect(&self) -> SkillEffect {
		let mut effect = SkillEffect::default();

		'lp: for n in 0..5 {
			let performer = &self.performers[n];
			let effect_limit = self.effect_limits[n];

			if effect_limit < self.time {
				continue 'lp;
			} else {
				effect = effect & performer.effect();
			}
		}

		effect
	}
}

pub trait Performer {
	fn appeal(&self) -> u16;
	fn interval(&self) -> f32;
	fn effect(&self) -> SkillEffect;
	fn activate(&self, rate: i8, life: i16) -> Option<Activity>;
}

pub struct Activity {
	pub length: f32,
	pub life: i16,
}

pub struct SkillMock {
	pub skill: Skill,
	threshold: f32,
	length: f32,
	interval: f32,
}

impl SkillMock {
	pub fn new(skill: Skill) -> Self {
		let t = (1.0 + 0.0555 * 9.0) * 1.3 * (skill.threshold_rate as f32);
		let l = (1.0 + 0.0555 * 9.0 * (skill.length as f32)) * 1000.0;
		let i = (skill.interval as f32)  * 1000.0;
		SkillMock {
			skill: skill,
			threshold: t,
			length: l,
			interval: i,
		}
	}
}

impl Performer for SkillMock {
	fn appeal(&self) -> u16 {
		20_000
	}

	fn interval(&self) -> f32 {
		self.interval
	}

	fn effect(&self) -> SkillEffect {
		self.skill.effect.clone()
	}

	fn activate(&self, rate: i8, life: i16) -> Option<Activity> {

		if life + self.skill.life_effect < 0 {
			None
		} else if (rate as f32) < self.threshold {
			Some(Activity {length: self.length, life: life + self.skill.life_effect})
		} else {
			None
		}
	}
}

impl Display for SkillMock {
	fn fmt(&self, f: &mut Formatter) -> Result {
		write!(f, "appeal: {}, skill: {} (rate {} per {} sec.)", self.appeal(), self.skill, self.threshold, self.length)
	}
}
