/// create a set of 'combinations with replacement'
/// ```
/// # use hinaculator::util::mod::create_combinations_with_replacement;
/// let combinations = create_combinations_with_replacement(10);
/// ```
pub fn create_combinations_with_replacement(element_count: u64) -> Vec<[u64; 5]> {
	fn add(current: [u64; 5], index: usize, base_value: u64, max_value: u64) -> Vec<[u64; 5]> {
		let next_index = index + 1;
		if index == 5 {
			vec!(current)
		} else if base_value + 1 == max_value {
			let mut current = current.clone();
			current[index] = base_value;
			add(current, next_index, base_value, max_value)
		} else {
			(base_value..max_value).flat_map(|next_value| {
				let mut current = current.clone();
				current[index] = next_value;
				add(current, next_index, next_value, max_value)
			}).collect()
		}
	}
	add([0; 5], 0, 0, element_count)
}

#[cfg(test)]
mod tests {
	use std::panic::catch_unwind;
	use std::thread::Result;
	use super::create_combinations_with_replacement;

	#[test]
	fn test_of_combinations_with_replacement_creator() {
		fn get_factorial(value: u64) -> u64 {
			(1..value + 1).fold(1, |acc, x| acc * x)
		}
		fn count_of_combinations_with_replacement(n: u64, r: u64) -> Result<u64> {
			catch_unwind(|| {
				get_factorial(n + r - 1) / get_factorial(n - 1) / get_factorial(r)
			})
		}
		for n in 1..17 {
			match count_of_combinations_with_replacement(n, 5) {
				Ok(expected) => assert_eq!(expected as usize, create_combinations_with_replacement(n).len()),
				Err(_) => assert!(n > 1000),
			}

		}
	}
}
